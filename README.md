---
# Mikrotik RouterOS user management with Ansible.

Look for examples in "docs" folder.

Big thanks to Martin Dulin for his role https://github.com/mikrotik-ansible/mikrotik-firewall.
His role gave me an idea how solve RouterOS configuration tasks.
