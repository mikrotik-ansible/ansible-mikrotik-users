---
Create users as in an example below.

Be careful: the role only sets defined variables.
If you set a variable, apply configuration and then
you undefine the variable - no any config changes will be done.
The value will remain the same as the last defined variable.

```
mikrotik_users:
  credentials_dir: 'credentials/'
  user_list:
    - name: u1 # login
      group: full # routeros group
      comment: u1 user # comment
      password: Pa$$word! # set password. Better use ansible-vault
      disabled: false # is user active

    - name: u2
      group: read
      comment: u2 user
      password: Pa$$word!
      address: 192.168.57.0/24 # allowed to login address list
      disabled: false

    - name: u3
      group: full
      comment: u3 user
      disabled: false
      # if password is not set it will be generated and put to {{credentials_dir}} + '/' + {{inventory_hostname}} + '__' + {{user.name}} + '__password.txt
      # the credentials_dir must exist or task fails

    - name: admin
      group: full
      comment: Admin user
      disabled: false
      password: Pa$$word!
      # paths to ssh public kery files
      pubkeys:
        - "{{playbook_dir}}/host_vars/vault/files/ssh_keys/admin1.pub"
```
